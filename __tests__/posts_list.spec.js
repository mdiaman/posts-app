import React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';
import PostsApp from '../src/PostsApp';
import {getPopulatedStore} from './helpers/mockStore';

const store = getPopulatedStore();

describe('<PostApp />', () => {
  it('should render and have a post by user @Second Test Me', () => {
    const tree = renderer.create(
      <Provider store={store}>
        <PostsApp />
      </Provider>,
    );

    expect(tree.toJSON()).toMatchSnapshot();
    expect(tree.root.findAllByType('Text')[8].children).toContain(
      '@Second Test Me: second test',
    );
  });
});
