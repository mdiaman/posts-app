import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const data = [
  {
    _id: '5e2a0a91523ea10011ac046b',
    content: 'test content',
    title: 'test',
    upvotes: 1,
    downvotes: 0,
    author: 'Test Me',
    lastModified: '2020-01-01T03:00:00.000Z',
  },
  {
    _id: '5e2a0a91523ea10011ac046c',
    content: 'second test content',
    title: 'second test',
    upvotes: 10,
    downvotes: 20,
    author: 'Second Test Me',
    lastModified: '2020-01-01T03:00:00.000Z',
  },
];

const getEmptyStore = () => mockStore({posts: []});
const getPopulatedStore = () => mockStore({posts: data});

export {getEmptyStore, getPopulatedStore, data};
