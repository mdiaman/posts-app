import Config from 'react-native-config';
import fetchMock from 'fetch-mock-jest';
import {fetchPosts} from '../src/store/services';
import {getEmptyStore, data} from './helpers/mockStore';

describe('/services async actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });
  it('dispatches FETCH_SUCCESS after fetching posts', () => {
    fetchMock.getOnce(`http://${Config.API_URL}:3000/posts`, {
      body: {posts: data},
    });
    const expectedActions = [
      {type: 'FETCH_INIT'},
      {type: 'FETCH_SUCCESS', posts: data},
    ];
    const store = getEmptyStore();
    return store.dispatch(fetchPosts()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
