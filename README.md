## PostsApp

### Running the application
The app is set up to run on the Android Emulator out of the box, which uses the address 10.0.2.2. Change the `API_URL` key under .env to run in a different environment.

1. Start the postsapp-server container

2. Run `yarn android`. 

### Testing

1. Run `yarn test` to start the jest test suite.

