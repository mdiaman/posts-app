import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Styles from './styles';

const ButtonSmall = ({title, action}) => {
  return (
    <TouchableOpacity style={Styles.btnSmall} onPress={action}>
      <Text style={Styles.btnSmallText}>{title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonSmall;
