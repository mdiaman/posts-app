import React, {useState, useEffect, useReducer} from 'react';
import {View, FlatList, ActivityIndicator, Text, Alert} from 'react-native';
import {connect} from 'react-redux';
import Post from './Post';
import ButtonNewPost from './ButtonNewPost';
import Styles from './styles';
import Colors from '../../constants/Colors';
import {removePost, fetchPosts} from '../../store/services';

const PostsList = props => {
  const {author, posts, isLoading, isError, remove} = props;
  const [content, setContent] = useState(posts);
  const canEdit = author === 'me';

  useEffect(() => {
    // Sort by votes
    let sortedPostsByAuthor = posts.sort((a, b) =>
      a.upvotes < b.upvotes ? 1 : -1,
    );

    // Filter by author
    sortedPostsByAuthor =
      author === 'me' ? posts.filter(post => post.author === 'Me') : posts;

    setContent(sortedPostsByAuthor);
  }, [author, posts]);

  if (isLoading) {
    return (
      <View style={Styles.loading}>
        <ActivityIndicator color={Colors.orange} size={'large'} />
      </View>
    );
  }

  if (isError) {
    return Alert.alert('#DummyApp encountered a problem fetching new posts.');
  }

  return (
    <View style={Styles.base}>
      {canEdit && (
        <View style={{alignItems: 'center'}}>
          <ButtonNewPost />
        </View>
      )}
      <FlatList
        data={content}
        keyExtractor={item => item._id}
        renderItem={({item}) => (
          <Post item={item} remove={remove} canEdit={canEdit} />
        )}
        ListEmptyComponent={
          <View style={Styles.emptyList}>
            <Text>No posts to display!</Text>
          </View>
        }
      />
    </View>
  );
};

const mapStateToProps = state => ({
  posts: state.posts,
  isLoading: state.isLoading,
  isError: state.isError,
});

const mapDispatchToProps = dispatch => ({
  remove: _id => dispatch(removePost(_id)),
  fetch: () => dispatch(fetchPosts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsList);
