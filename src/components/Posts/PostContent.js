import React, {useState} from 'react';
import {connect} from 'react-redux';
import {TextInput, View} from 'react-native';
import Styles from './styles';
import ButtonSmall from './ButtonSmall';
import {addPost, updatePost} from '../../store/services';

const PostContent = props => {
  const {action, post, goBack, add, update} = props;
  const [title, setTitle] = useState(post ? post.title : '');
  const [content, setContent] = useState(post ? post.content : '');

  const handleTitle = text => setTitle(text);
  const handleContent = text => setContent(text);
  const back = () => goBack();
  const save = () => {
    action === 'update'
      ? update({
          author: post.author,
          _id: post._id,
          title: title,
          content: content,
          lastModified: new Date(),
          upvotes: post.upvotes,
          downvotes: post.downvotes,
        })
      : add({title, content, author: 'Me'});
    goBack();
  };

  return (
    <View style={Styles.contentContainer}>
      <TextInput
        textAlignVertical={'top'}
        value={title}
        onChangeText={handleTitle}
        placeholder={'Title goes here...'}
        placeholderTextColor={'#000'}
        style={{flex: 1, padding: 10, marginTop: 8, fontSize: 16}}
      />
      <TextInput
        multiline
        textAlignVertical={'top'}
        value={content}
        onChangeText={handleContent}
        placeholder={'Content goes here'}
        placeholderTextColor={'#000'}
        style={{flex: 5, padding: 10, fontSize: 16}}
      />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
        }}>
        <ButtonSmall action={back} title={'Back'} />
        <ButtonSmall action={save} title={'Save'} />
      </View>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  update: post => dispatch(updatePost(post)),
  add: post => dispatch(addPost(post)),
});

export default connect(null, mapDispatchToProps)(PostContent);
