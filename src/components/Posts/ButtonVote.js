import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../../constants/Colors';

const ButtonVote = ({direction, votes, action}) => {
  const color = direction === 'up' ? 'green' : 'red';
  return (
    <TouchableOpacity
      style={{
        flex: 1,
        flexDirection: 'row',
      }}
      onPress={action}>
      <Icon
        name={`arrow-${direction}`}
        size={16}
        color={color}
        style={{
          paddingVertical: 3,
        }}
      />
      <Text style={{color: Colors.darkGray, fontSize: 16}}>{votes}</Text>
    </TouchableOpacity>
  );
};

export default ButtonVote;
