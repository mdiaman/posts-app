import React from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import moment from 'moment';
import Colors from '../../constants/Colors';
import Styles from './styles';
import ButtonVote from './ButtonVote';
import {upvote, downvote} from '../../store/services';

const PostFooter = ({post, up, down}) => {
  const time = moment(post.lastModified).fromNow();
  const handleUpvote = () =>
    up({
      author: post.author,
      _id: post._id,
      title: post.title,
      content: post.content,
      lastModified: new Date(),
      upvotes: post.upvotes + 1,
      downvotes: post.downvotes,
    });
  const handleDownvote = () =>
    down({
      author: post.author,
      _id: post._id,
      title: post.title,
      content: post.content,
      lastModified: new Date(),
      upvotes: post.upvotes,
      downvotes: post.downvotes + 1,
    });

  return (
    <View style={{flexDirection: 'row'}}>
      <ButtonVote direction="up" votes={post.upvotes} action={handleUpvote} />
      <ButtonVote
        direction="down"
        votes={post.downvotes}
        action={handleDownvote}
      />
      <View style={Styles.time}>
        <Text style={{color: Colors.gray}}>{time}</Text>
      </View>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  up: post => dispatch(upvote(post)),
  down: post => dispatch(downvote(post)),
});

export default connect(null, mapDispatchToProps)(PostFooter);
