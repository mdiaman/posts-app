import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PostFooter from './PostFooter';
import {withNavigation} from 'react-navigation';
import Styles from './styles';

const Post = props => {
  const {
    _id,
    title,
    content,
    upvotes,
    downvotes,
    author,
    lastModified,
  } = props.item;
  const {canEdit, remove} = props;

  const cardTitle = `@${author}: ${title}`;

  return (
    <View style={Styles.post}>
      {canEdit && (
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          <TouchableOpacity
            style={Styles.base}
            onPress={() =>
              props.navigation.navigate('PostScreen', {post: props.item})
            }>
            <Icon name="pencil" size={20} color="gray" />
          </TouchableOpacity>
          <TouchableOpacity style={Styles.base} onPress={() => remove(_id)}>
            <Icon name="remove" size={20} color="gray" />
          </TouchableOpacity>
        </View>
      )}
      <View style={{alignSelf: 'flex-start', flex: 2}}>
        <Text style={{fontSize: 18, fontWeight: 'bold'}}>{cardTitle}</Text>
      </View>
      <View style={{flex: 3}}>
        <Text style={{fontSize: 16, marginVertical: 10, marginHorizontal: 4}}>
          {content}
        </Text>
      </View>
      <PostFooter
        post={props.item}
        upvotes={upvotes}
        downvotes={downvotes}
        lastModified={lastModified}
      />
    </View>
  );
};

export default withNavigation(Post);
