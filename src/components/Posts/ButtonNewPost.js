import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {withNavigation} from 'react-navigation';
import Styles from './styles';

const ButtonNewPost = ({navigation}) => (
  <TouchableOpacity
    style={Styles.btnNew}
    onPress={() => navigation.navigate('PostScreen')}>
    <Icon
      name="plus-circle"
      size={19}
      color="#fff"
      style={{paddingVertical: 8}}
    />
    <Text style={Styles.btnNewText}>NEW POST</Text>
  </TouchableOpacity>
);

export default withNavigation(ButtonNewPost);
