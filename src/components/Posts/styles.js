import {StyleSheet, Dimensions} from 'react-native';
import Colors from '../../constants/Colors';
const {width, height} = Dimensions.get('window');

const Styles = StyleSheet.create({
  base: {flex: 1},
  btnNew: {
    borderRadius: 25,
    flexDirection: 'row',
    width: width * 0.5,
    backgroundColor: Colors.orange,
    justifyContent: 'center',
    marginVertical: 6,
    padding: 5,
  },
  btnNewText: {
    color: '#fff',
    paddingStart: 4,
    fontSize: 18,
    paddingVertical: 5,
  },
  btnSmall: {
    backgroundColor: Colors.orange,
    padding: 8,
    borderRadius: 16,
  },
  btnSmallText: {fontSize: 16, color: '#fff'},
  time: {
    flex: 3,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyList: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  contentContainer: {flex: 1, margin: 8, backgroundColor: '#f3f3f3'},
  post: {
    padding: 5,
    marginBottom: 10,
    borderBottomColor: '#bbb',
    borderBottomWidth: 1,
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    marginTop: 6,
  },
});

export default Styles;
