import React from 'react';
import {View, Text} from 'react-native';
import Styles from './styles';

const Header = () => {
  return (
    <View style={Styles.background}>
      <Text style={Styles.text}>#DummyApp</Text>
    </View>
  );
};

export default Header;
