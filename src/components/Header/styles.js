import {StyleSheet, Platform} from 'react-native';

const Styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#18a999',
    paddingTop: Platform.OS === 'ios' ? 30 : 10,
  },
  text: {color: '#fff', fontSize: 40, fontWeight: 'bold'},
});

export default Styles;
