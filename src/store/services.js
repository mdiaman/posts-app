import Config from 'react-native-config';
import {Alert} from 'react-native';
import {fetchInit, fetchSuccess, fetchFailure, update, remove} from './actions';

const fetchPosts = () => async dispatch => {
  dispatch(fetchInit());
  try {
    const result = await fetch(`http://${Config.API_URL}:3000/posts`);
    const response = await result.json();
    if (response.posts) {
      dispatch(fetchSuccess(response.posts));
    } else {
      dispatch(fetchFailure());
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    dispatch(fetchFailure());
  }
};

const updatePost = post => async dispatch => {
  const results = await fetch(
    `http://${Config.API_URL}:3000/posts/${post._id}`,
    {
      method: 'PUT',
      body: JSON.stringify(post),
      headers: {'Content-Type': 'application/json'},
    },
  );

  if (results.ok) {
    dispatch(update(post));
  } else {
    Alert.alert('Problem updating post');
  }
};

const upvote = post => async dispatch => {
  const results = await fetch(`http://${Config.API_URL}:3000/posts/vote`, {
    method: 'POST',
    body: JSON.stringify({field: 'upvotes', post: post}),
    headers: {'Content-Type': 'application/json'},
  });

  if (results.ok) {
    dispatch(update(post));
  } else {
    Alert.alert('Problem updating post');
  }
};

const downvote = post => async dispatch => {
  const results = await fetch(`http://${Config.API_URL}:3000/posts/vote`, {
    method: 'POST',
    body: JSON.stringify({field: 'downvotes', post: post}),
    headers: {'Content-Type': 'application/json'},
  });

  if (results.ok) {
    dispatch(update(post));
  } else {
    Alert.alert('Problem updating post');
  }
};
const removePost = _id => async dispatch => {
  const results = await fetch(`http://${Config.API_URL}:3000/posts/${_id}`, {
    method: 'DELETE',
  });
  if (results.ok) {
    dispatch(remove(_id));
  } else {
    Alert.alert('Problem removing post');
  }
};

const addPost = post => async dispatch => {
  const results = await fetch(`http://${Config.API_URL}:3000/posts/`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(post),
  });
  if (results.ok) {
    dispatch(fetchPosts());
  } else {
    Alert.alert('Problem adding post');
  }
};

export {addPost, removePost, downvote, upvote, updatePost, fetchPosts};
