import update from 'immutability-helper';

const initialState = {
  isLoading: false,
  isError: false,
  posts: [],
};

const postsReducer = (state = initialState, action) => {
  let index;

  switch (action.type) {
    case 'FETCH_INIT':
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case 'FETCH_SUCCESS':
      return {
        isLoading: false,
        isError: false,
        posts: action.posts,
      };
    case 'FETCH_FAILURE':
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    case 'UPDATE_POST':
      index = state.posts.findIndex(post => post._id === action.post._id);
      return update(state, {posts: {[index]: {$set: action.post}}});

    case 'ADD_POST':
      return update(state, {posts: {$push: [action.post]}});

    case 'DELETE_POST':
      index = state.posts.findIndex(post => post._id === action._id);
      return update(state, {posts: {$splice: [[index, 1]]}});

    default:
      return state;
  }
};

export default postsReducer;
