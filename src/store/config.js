import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import postsReducer from './postsReducer';

const middlewares = [thunk];

const enhancer = applyMiddleware(...middlewares);

const store = createStore(postsReducer, enhancer);

export default store;
