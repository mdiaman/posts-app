const fetchInit = () => ({
  type: 'FETCH_INIT',
});

const fetchSuccess = posts => ({
  type: 'FETCH_SUCCESS',
  posts,
});

const fetchFailure = () => ({
  type: 'FETCH_FAILURE',
});

const add = post => ({
  type: 'ADD_POST',
  post,
});

const update = post => ({
  type: 'UPDATE_POST',
  post,
});

const remove = _id => ({
  type: 'DELETE_POST',
  _id,
});

export {add, fetchFailure, fetchInit, fetchSuccess, remove, update};
