import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import AppNavigator from './navigation/AppNavigator';
import {fetchPosts} from './store/services';

const PostsApp = props => {
  useEffect(() => {
    props.fetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <AppNavigator />;
};

const mapDispatchToProps = dispatch => ({
  fetch: () => dispatch(fetchPosts()),
});

export default connect(null, mapDispatchToProps)(PostsApp);
