import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import UserContentScreen from '../containers/User/UserContentScreen';
import PostContentScreen from '../containers/User/PostContentScreen';
import HomeScreen from '../containers/Home/HomeScreen';

const UserContentStack = createStackNavigator(
  {UserScreen: UserContentScreen, PostScreen: PostContentScreen},
  {initialRouteName: 'UserScreen'},
);

const MainNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: 'Feed',
        tabBarIcon: ({tintColor}) => (
          <Icon name="feed" color={tintColor} size={25} />
        ),
      },
    },
    UserContent: {
      screen: UserContentStack,
      navigationOptions: {
        tabBarLabel: 'Your Posts',
        tabBarIcon: ({tintColor}) => (
          <Icon name="newspaper-o" color={tintColor} size={25} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: '#2b2d42',
      inactiveTintColor: '#8d99ae',
      labelStyle: {
        fontSize: 14,
        fontWeight: 'bold',
      },
      style: {
        backgroundColor: '#eee',
        height: 70,
      },
    },
  },
);

export default createAppContainer(MainNavigator);
