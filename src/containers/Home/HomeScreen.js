import React from 'react';
import {View} from 'react-native';
import Header from '../../components/Header/Header';
import PostsList from '../../components/Posts/PostsList';
import Styles from './styles';
import {withNavigationFocus} from 'react-navigation';

const HomeScreen = () => {
  return (
    <View style={Styles.container}>
      <Header style={Styles.header} />
      <View style={Styles.postsContainer}>
        <PostsList author="all" />
      </View>
    </View>
  );
};
export default withNavigationFocus(HomeScreen);
