import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
  container: {flex: 1},
  postsContainer: {flex: 6, backgroundColor: '#f5f5f5'},
  header: {flex: 2},
});

export default Styles;
