import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
  container: {flex: 1},
  header: {flex: 2},
  postsContainer: {
    backgroundColor: '#ccc',
    flex: 6,
  },
  usersContainer: {
    flex: 6,
  },
});

export default Styles;
