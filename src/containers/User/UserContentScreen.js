import React from 'react';
import {View} from 'react-native';
import Header from '../../components/Header/Header';
import PostsList from '../../components/Posts/PostsList';
import Styles from './styles';

const UserContentScreen = () => {
  return (
    <View style={Styles.container}>
      <Header style={Styles.header} />
      <View style={Styles.usersContainer}>
        <PostsList author="me" />
      </View>
    </View>
  );
};

UserContentScreen.navigationOptions = {
  headerShown: false,
};

export default UserContentScreen;
