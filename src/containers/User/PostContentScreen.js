import React from 'react';
import {View} from 'react-native';
import Header from '../../components/Header/Header';
import PostContent from '../../components/Posts/PostContent';
import Styles from './styles';

const PostContentScreen = props => {
  const post = props.navigation.getParam('post');
  console.log(`post param ${JSON.stringify(post)}`);

  return (
    <View style={Styles.container}>
      <Header style={Styles.header} />
      <View style={Styles.postsContainer}>
        <PostContent
          goBack={props.navigation.goBack}
          action={post ? 'update' : 'create'}
          post={post}
        />
      </View>
    </View>
  );
};

PostContentScreen.navigationOptions = {
  headerShown: false,
};

export default PostContentScreen;
