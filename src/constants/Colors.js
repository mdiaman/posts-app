const Colors = {
  gray: '#6d6875',
  orange: '#FF8124',
  darkGray: '#2b2d42',
};

export default Colors;
