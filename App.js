import React from 'react';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import PostsApp from './src/PostsApp';
import store from './src/store/config';

const App = () => {
  return (
    <Provider store={store}>
      <PostsApp />
    </Provider>
  );
};

export default App;
